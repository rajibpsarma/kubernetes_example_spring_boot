# Spring Boot App deployed on Kubernetes Cluster #

An example of a simple Spring Boot app deployed to Kubernetes (Minicube) Cluster.

### The main points are: ###
* The source code of the Spring Boot app used here is at: https://bitbucket.org/rajibpsarma/docker_example_spring_boot
* It uses deployment.yaml file to create the deployment on Minicube.
* The yaml file is generated using command "kubectl create deployment docker-example-spring-boot --image=rajibsarma/docker_example_spring_boot --dry-run=client -o=yaml > deployment.yaml"
* The deployment is created using command "kubectl apply -f deployment.yaml"
* Another way of creating the deployment is "kubectl create deployment docker-example-spring-boot --image=rajibsarma/docker_example_spring_boot"
* Expose the deployment to outside world using a service: kubectl expose deployment/docker-example-spring-boot --type="NodePort" --port 8080
* Check the deployment and service status using "kubectl get all".
* Determine the IP of cluster using command "kubectl cluster-info".
* Determine the nodePort the service is running using command "kubectl describe service/docker-example-spring-boot"
* Access the Spring app in the browser using URL "http://<Cluster_IP>:<nodePort_of_the_service>/"
